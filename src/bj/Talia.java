package bj;

import java.util.Random;

public class Talia {
	public static final int ile_kart = 52*4;
	public int tablica[] = new int[ile_kart];
	public int ile_zostalo_w_talii = ile_kart;
	public static Random r = new Random();
		
	public Talia(){
		for(int i = 0; i < ile_kart; i++)
		{
			tablica[i] = 1;
		}
		
	}
	
	public int losuj(){
		
		int a;
		
		do{
			a = r.nextInt(ile_kart);
		}while( tablica[a] == 0); // jezeli false, to wyjdzie
		
		tablica[a] = 0;
		ile_zostalo_w_talii--;
		//System.out.println(a);
		jaka_karta(a);
		return a;
		
	}
	
	
	void jaka_karta(int a){
		char znak;
		char kolor = 'a';
		int temp = 48+(a%13)+2;
		znak = (char) (temp);
		
		switch(temp){
		case 58: znak = 'T'; break;
		case 59: znak = 'J'; break;
		case 60: znak = 'Q'; break;
		case 61: znak = 'K'; break;
		case 62: znak = 'A'; break;
		}
		
		if(a < ile_kart)
			kolor = 'h';
		if(a > ile_kart/4 - 1)
			kolor = 'c';
		if(a > ile_kart/2 - 1)
			kolor = 's';
		if(a > ile_kart*3/4 - 1)
			kolor = 'd';
		System.out.print(znak + "" + kolor + " ");
	}

}
